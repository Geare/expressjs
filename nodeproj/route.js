const express = require('express');
const router=express.Router();
var bodyparser = require('body-parser');
router.use(bodyparser.json());
router.use(bodyparser.urlencoded({extended: true}));

router.use(express.static('public'));
router.get("/",function(request,response){
    response.status(200);
    response.sendFile(__dirname+'/public/input.html')
})
router.get("/hello",function(request,response){
    response.status(200);
    response.sendFile(__dirname+'/public/input.html')
});
router.get("/home",function(request,response){
    response.status(200);
    response.sendFile(__dirname+'/public/input.html')
});
router.post('/',function(request,response){
    response.send(JSON.stringify(request.body))
});
module.exports=router;